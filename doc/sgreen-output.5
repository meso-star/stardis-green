.\" Copyright (C) 2020-2022, 2024 |Méso|Star> (contact@meso-star.com)
.\"
.\" This program is free software: you can redistribute it and/or modify
.\" it under the terms of the GNU General Public License as published by
.\" the Free Software Foundation, either version 3 of the License, or
.\" (at your option) any later version.
.\"
.\" This program is distributed in the hope that it will be useful,
.\" but WITHOUT ANY WARRANTY; without even the implied warranty of
.\" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
.\" GNU General Public License for more details.
.\"
.\" You should have received a copy of the GNU General Public License
.\" along with this program. If not, see <http://www.gnu.org/licenses/>.
.Dd April 26, 2024
.Dt SGREEN-OUTPUT 5
.Os
.\""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
.Sh NAME
.Nm sgreen-output
.Nd output format of
.Xr sgreen 1
results
.\""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
.Sh DESCRIPTION
The type of the data that are generated depends on the options used when
.Xr sgreen 1
is invoked.
When invoked with option
.Fl a ,
.Xr sgreen 1
outputs Monte Carlo results, either in extended or compact format,
whether option
.Fl e
is used or not.
.Pp
When invoked with the
.Fl s
option,
.Xr sgreen 1
silently creates an HTML file containing a summary of the Green function
used.
As it conforms to the HTML format found in the HTML specification
documents, these will not be described in further detail below.
This summary HTML file contains the following sections:
.Bl -bullet -compact -offset indent
.It
List of variables
.It
Formula of the Monte Carlo estimate
.It
Counts
.It
Solids
.It
Fluids
.It
T boundaries for solids
.It
T boundaries for fluids
.It
H boundaries for solids
.It
H boundaries for fluids
.It
F boundaries for solids
.It
Solid-Fluid connections
.It
Solid-Solid connections
.It
Radiative temperatures
.El
.Pp
In what follows, some lines end with a backslash
.Pq Li \e .
This is used as a convenience to continue a description next line.
However, this trick cannot be used in actual description files and
actual description lines must be kept single-line.
Text introduced by the sharp character
.Pq Li #
in descriptions is a comment and is not part of the description.
.Pp
The file format for Monte Carlo results is as follows:
.Bl -column (****************) (::) ()
.It Ao Va results Ac Ta ::= Ta Ao Va results-raw Ac | Ao Va results-ext Ac
.It Ao Va results-raw Ac Ta ::= Ta Ao Va estimate-raw Ac
.It Ta Ta ...
.It Ao Va results-ext Ac Ta ::= Ta Li message: Initialisation time = Aq Va time
.It Ta Ta Li message: Computation time = Aq Va time
.It Ta Ta Ao Va estimate-ext Ac
.It Ta Ta ...
.It Ao Va estimate-raw Ac Ta ::= Ta Ao Va expected-value Ac Ao Va standard-error Ac
.It Ao Va estimate-ext Ac Ta ::= Ta Ao Va expected-value Ac Li K +/- Ao Va standard-error Ac \e
.It Ta Ta Ao Va applied-settings Ac
.It \  Ta Ta
.It Ao Va expected-value Ac Ta ::= Ta Vt real
.It Ao Va standard-error Ac Ta ::= Ta Vt real
.It Ao Va time Ac Ta :: Ta
.Op Ao Va h Ac Li hour
.Op Ao Va m Ac Li min
.Op Ao Va s Ac Li sec
\e
.It Ta Ta
.Op Ao Va ms Ac Li msec
.Op Ao Va us Ac Li usec
.It Ao Va h Ac Ta ::= Ta Vt integer No # \&In Bq 1, INF
.It Ao Va m Ac Ta ::= Ta Vt integer No # \&In Bq 1, 59
.It Ao Va s Ac Ta ::= Ta Vt integer No # \&In Bq 1, 59
.It Ao Va ms Ac Ta ::= Ta Vt integer No # \&In  Bq 1, 999
.It Ao Va us Ac Ta ::= Ta Vt integer No # \&In Bq 1, 999
.It Ao Va applied-settings Ac Ta ::= Ta Vt string No # Verbatim from the settings file
.El
.\""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
.Sh SEE ALSO
.Xr sgreen 1
.\""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
.Sh STANDARDS
.Rs
.%A Web Hypertext Application Technology Working Group
.%T HTML Living Standard
.%U https://html.spec.whatwg.org/
.Re
