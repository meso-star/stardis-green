# Stardis Green

Stardis Green is a postprocess of Green function binary files, as
produced by the [Stardis](https://gitlab.com/meso-star/stardis) program.
Its main purpose is to compute Monte Carlo results by applying Green
functions to new settings, enabling ultra-fast Monte Carlo computations.

## Requirements

- C compiler with OpenMP support
- POSIX make
- pkg-config
- [RSys](https://gitlab.com/vaplv/rsys)
- [Stardis](https://gitlab.com/meso-star/stardis)

## Installation

Edit config.mk as needed, then run:

    make clean install

## Release notes

### Version 0.5.1

Fix incorrect installation directory for license and README files.

### Version 0.5

- Write the man pages directly in mdoc's roff macros, instead of using
  the intermediate asciidoc source.
- Replace CMake by Makefile as build system.
- Update compiler and linker flags to increase the security and
  robustness of generated binaries.
- Provide a pkg-config file to link the library as an external
  dependency.

### Version 0.4

- Change input file format to cope with stardis changes.
- Now depends on the stardis-green-types.h header file, that is
  installed when building stardis and describes the types used to read
  and write Green function binary files. Currently depends on version 4.

### Version 0.3

- Change input file format to cope with stardis changes.

### Version 0.2.1

Fix a bug that could cause a crash.

### Version 0.2

- Add connections to html output.
- Add compute time information to output.
- Adapt to new file format from stardis.

### Version 0.1.1

- Check binary Green file format version (this require stardis 0.5.1).
- Man improvement.
- Ensure C89 compatibility.

### Version 0.1

- Compatible with stardis v0.5.0 binary Green function files.
- Can produce a summary of a Green function in HTML format.
- Can apply a Green function to new settings and output the
  corresponding Monte Carlo results.

## License

Copyright (C) 2020-2022, 2024 |Méso|Star> (contact@meso-star.com).

Stardis-Green is free software released under the GPL v3+ license: GNU
GPL version 3 or later.  You are welcome to redistribute it under
certain conditions; refer to the COPYING file for details.
