/* Copyright (C) 2020-2022, 2024 |Méso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef GREEN_ARGS_H
#define GREEN_ARGS_H

#include <rsys/rsys.h>

struct logger;
struct mem_allocator;
struct args;

struct args {
  struct logger* logger;
  struct mem_allocator* allocator;
  char* green_filename;
  char* command_filename;
  char* info_filename;
  unsigned nthreads;
  int mode;
  int verbose;
};

void
init_args
  (struct logger* logger,
   struct mem_allocator* allocator,
   struct args* args);

res_T
parse_args
  (const int argc,
   char** argv,
   struct args* args);

#endif /* GREEN_ARGS_H */
