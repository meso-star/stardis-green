/* Copyright (C) 2020-2022, 2024 |Méso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef GREEN_TYPES_H
#define GREEN_TYPES_H

#include <rsys/str.h>
#include <rsys/mem_allocator.h>
#include <rsys/hash_table.h>
#include <rsys/dynamic_array.h>
#include <rsys/dynamic_array_char.h>
#include <rsys/dynamic_array_str.h>
#include <stardis/stardis-green-types.h>

#include <string.h>
#include <omp.h>

/* Utility macros */
#define ERR(Expr) if((res = (Expr)) != RES_OK) goto error; else (void)0

#define CHK_TOK(x, Name) if((tk = (x)) == NULL) {\
   logger_print(green->logger, LOG_ERROR,\
     "Invalid data (missing " Name ")\n");\
   res = RES_BAD_ARG;\
   goto error;\
  } (void)0

#ifdef COMPILER_CL
#define strtok_r strtok_s
#endif

static INLINE void
log_err(const char* msg, void* ctx)
{
  ASSERT(msg);
  (void)ctx;
#ifdef OS_WINDOWS
  fprintf(stderr, "error: %s", msg);
#else
  fprintf(stderr, "\x1b[31merror:\x1b[0m %s", msg);
#endif
}

static INLINE void
log_warn(const char* msg, void* ctx)
{
  ASSERT(msg);
  (void)ctx;
#ifdef OS_WINDOWS
  fprintf(stderr, "warning: %s", msg);
#else
  fprintf(stderr, "\x1b[33mwarning:\x1b[0m %s", msg);
#endif
}

static INLINE void
log_prt(const char* msg, void* ctx)
{
  ASSERT(msg);
  (void)ctx;
#ifdef OS_WINDOWS
  fprintf(stderr, "message: %s", msg);
#else
  fprintf(stderr, "\x1b[32moutput:\x1b[0m %s", msg);
#endif
}

/*
 * A type for green mode
 */
enum green_mode {
  MODE_NULL = 0,
  MODE_APPLY_GREEN = BIT(1), /* -a */
  MODE_EXTENTED_RESULTS = BIT(2), /* -e */
  MODE_READ_GREEN = BIT(3), /* -g */
  MODE_HELP = BIT(4), /* -h */
  MODE_HTML_SUMMARY = BIT(5), /* -s */
  MODE_THREADS_COUNT = BIT(6), /* -v */
  MODE_VERSION = BIT(7), /* -v */
  MODE_VERBOSE = BIT(8) /* -v */
};

/*
 * The type to store a green sample
 */
struct sample {
  struct green_sample_header header;
  unsigned* pw_ids;
  double* pw_weights;
  unsigned* fx_ids;
  double* fx_weights;
};

static INLINE res_T
alloc_sample_buffers
  (struct mem_allocator* alloc, 
   struct sample* sample)
{
  unsigned* ids = NULL;
  double* weights = NULL;
  size_t count;

  ASSERT(alloc && sample);
  count =
    sample->header.pw_count + sample->header.fx_count;
  ids = MEM_ALLOC(alloc, count * sizeof(*ids));
  weights = MEM_ALLOC(alloc, count * sizeof(*sample->pw_weights));

  if(!ids || !weights) {
    MEM_RM(alloc, ids);
    MEM_RM(alloc, weights);
    return RES_MEM_ERR;
  }
  sample->pw_ids = ids;
  sample->pw_weights = weights;
  sample->fx_ids = ids + sample->header.pw_count;
  sample->fx_weights = weights + sample->header.pw_count;

  return RES_OK;
}

#define READ_SAMPLE_BUFFER(Sample) { \
  size_t count = (Sample)->header.pw_count + (Sample)->header.fx_count; \
  FR((Sample)->pw_ids, count); \
  FR((Sample)->pw_weights, count); \
} (void)0

static INLINE void
release_sample
  (struct mem_allocator* alloc,
   struct sample* sample)
{
  ASSERT(alloc && sample);
  MEM_RM(alloc, sample->pw_ids);
  MEM_RM(alloc, sample->pw_weights);
  /* Don't RM fx_ids nor fx_weights as they share pw buffers */
}

/*
 * Types used to compute green
 */

struct applied_settings_elt {
  double imposed_T_value, initial_T_value;
  double other_value;
};

/* other is used both for power and flux, as no description can have both */
struct table_elt {
  struct str imposed_T_name, initial_T_name, other_name;
  double imposed_T_value, initial_T_value, other_value;
  int imposed_T_defined, initial_T_defined, other_defined;
  int imposed_T_unknown, initial_T_unknown, other_unknown;
  unsigned imposed_T_weight, initial_T_weight;
  double other_weight;
};

static INLINE void
init_table_elt
  (struct mem_allocator* alloc,
   struct table_elt* elt)
{
  ASSERT(alloc && elt);
  str_init(alloc, &elt->imposed_T_name);
  str_init(alloc, &elt->initial_T_name);
  str_init(alloc, &elt->other_name);
  elt->imposed_T_value = 0;
  elt->initial_T_value = 0;
  elt->other_value = 0;
  elt->imposed_T_defined = 0;
  elt->initial_T_defined = 0;
  elt->other_defined = 0;
  elt->imposed_T_unknown = 0;
  elt->initial_T_unknown = 0;
  elt->other_unknown = 0;
  elt->imposed_T_weight = 0;
  elt->initial_T_weight = 0;
  elt->other_weight = 0;
}

static INLINE void
release_table_elt
  (struct table_elt* elt)
{
  ASSERT(elt);
  str_release(&elt->imposed_T_name);
  str_release(&elt->initial_T_name);
  str_release(&elt->other_name);
}

struct variable_data {
  size_t idx;
  int used;
  int unknown;
};

#define HTABLE_NAME variable_ptr
#define HTABLE_DATA struct variable_data
#define HTABLE_KEY struct str
#define HTABLE_KEY_FUNCTOR_HASH str_hash
#define HTABLE_KEY_FUNCTOR_EQ str_eq
#define HTABLE_KEY_FUNCTOR_INIT str_init
#define HTABLE_KEY_FUNCTOR_COPY str_copy
#define HTABLE_KEY_FUNCTOR_RELEASE str_release
#define HTABLE_KEY_FUNCTOR_COPY_AND_RELEASE str_copy_and_release
#include <rsys/hash_table.h>

struct result {
  double E, STD;
};

#define DARRAY_NAME result
#define DARRAY_DATA struct result
#include <rsys/dynamic_array.h>

/*
 * Main type to store binary file content
 */
struct green {
  struct mem_allocator* allocator;
  struct logger* logger;
  struct green_file_header header;
  struct green_description* descriptions;
  struct table_elt* table;
  struct sample* samples;
  int references_checked, unused_variables, unknown_variables;
  unsigned nthreads;
  struct htable_variable_ptr variable_ptrs;
  struct darray_str settings;
  struct darray_result results;
};

static INLINE void
green_init
  (struct mem_allocator* alloc,
   struct logger* logger,
   const unsigned nthreads,
   struct green* green)
{
  ASSERT(alloc && green);
  green->allocator = alloc;
  green->logger = logger;
  green->descriptions = NULL;
  green->table = NULL;
  green->samples = NULL;
  green->references_checked = 0;
  green->unused_variables = 0;
  green->unknown_variables = 0;
  green->nthreads = MMIN(nthreads, (unsigned)omp_get_num_procs());
  htable_variable_ptr_init(alloc, &green->variable_ptrs);
  darray_str_init(alloc, &green->settings);
  darray_result_init(alloc, &green->results);
}

static INLINE void
green_release
  (struct green* green)
{
  unsigned i;
  ASSERT(green);

  MEM_RM(green->allocator, green->descriptions);
  if(green->samples) { /* On error, samples could be not yet allocated */
    FOR_EACH(i, 0, green->header.ok_count)
      release_sample(green->allocator, green->samples + i);
    MEM_RM(green->allocator, green->samples);
  }
  if(green->table) { /* On error, table could be not yet allocated */
    FOR_EACH(i, 0, 1 + green->header.description_count)
      release_table_elt(&green->table[i]);
    MEM_RM(green->allocator, green->table);
  }
  green->allocator = NULL;
  htable_variable_ptr_release(&green->variable_ptrs);
  darray_str_release(&green->settings);
  darray_result_release(&green->results);
}

res_T
read_green_function
  (struct green* green,
   FILE* stream);

res_T
green_read_settings
  (struct green* green,
   const char* in_name);

#endif /* GREEN_TYPES_H */
