/* Copyright (C) 2020-2022, 2024 |Méso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "green-compute.h"
#include "green-types.h"

#include <rsys/rsys.h>
#include <rsys/str.h>
#include <rsys/logger.h>
#include <rsys/mem_allocator.h>
#include <rsys/text_reader.h>

#include <stdlib.h>

#define FR(Ptr, Count) \
  if((Count) != fread((Ptr), sizeof(*(Ptr)), (Count), stream)) { \
    logger_print(green->logger, LOG_ERROR, \
      "Could not read expected data.\n"); \
    res = RES_IO_ERR; \
    goto error; \
  }

static res_T
read_sample
  (struct sample* sample,
   struct mem_allocator* alloc,
   struct green* green,
   FILE* stream)
{
  res_T res = RES_OK;
  ASSERT(sample && stream);
  
  /* Read counts */
  FR(&sample->header, 1);
  /* Alloc buffers */
  ERR(alloc_sample_buffers(alloc, sample));
  /* Read Ids and weights */
  READ_SAMPLE_BUFFER(sample);

end:
  return res;
error:
  release_sample(alloc, sample);
  goto end;
}

res_T
read_green_function
  (struct green* green,
   FILE* stream)
{
  res_T res = RES_OK;
  unsigned i;

  ASSERT(green && stream);

  /* Read header */
  FR(&green->header, 1);

  /* Check Green string and file format version */
  if(strcmp(green->header.green_string, BIN_FILE_IDENT_STRING)) {
    logger_print(green->logger, LOG_ERROR,
      "File is not a binary Green file.\n");
    res = RES_BAD_ARG;
    goto error;
  }
  if(green->header.file_format_version != GREEN_FILE_FORMAT_VERSION) {
    logger_print(green->logger, LOG_ERROR,
      "Incompatible file format version (%u VS expected %u).\n",
      green->header.file_format_version, GREEN_FILE_FORMAT_VERSION);
    res = RES_BAD_ARG;
    goto error;
  }

  /* Check counts */
  if(green->header.description_count !=
    (green->header.solid_count + green->header.fluid_count
      + green->header.tbound_count + green->header.hbound_count
      + green->header.fbound_count + green->header.sfconnect_count
      + green->header.ssconnect_count))
  {
    logger_print(green->logger, LOG_ERROR,
      "Inconsistant description counts (%u).\n",
      green->header.description_count);
    res = RES_BAD_ARG;
    goto error;
  }

  /* Read descriptions*/
  green->descriptions = MEM_CALLOC(green->allocator,
    green->header.description_count, sizeof(*green->descriptions));
  if(!green->descriptions) {
    res = RES_MEM_ERR;
    goto error;
  }
  FR(green->descriptions, green->header.description_count);

  /* Read samples*/
  green->samples = MEM_CALLOC(green->allocator, green->header.ok_count,
    sizeof(*green->samples));
  if(!green->samples) {
    res = RES_MEM_ERR;
    goto error;
  }
  FOR_EACH(i, 0, green->header.ok_count) {
    ERR(read_sample(green->samples + i, green->allocator, green, stream));
  }

  /* Build table */
  ERR(build_green_table(green));

end:
  return res;
error:
  goto end;
}

res_T
green_read_settings
  (struct green* green,
   const char* in_name)
{
  res_T res = RES_OK;
  struct txtrdr* txtrdr = NULL;
  FILE* stream = NULL;

  ASSERT(green && in_name);

  stream = fopen(in_name, "r");
  if(!stream) {
    logger_print(green->logger, LOG_ERROR,
      "Cannot open settings file '%s'\n",
      in_name);
    res = RES_IO_ERR;
    goto error;
  }
  txtrdr_stream(green->allocator, stream, in_name, '#', &txtrdr);
  for(;;) {
    char* line;
    size_t sz;
    ERR(txtrdr_read_line(txtrdr));
    line = txtrdr_get_line(txtrdr);
    if(!line) break;
    sz = darray_str_size_get(&green->settings);
    ERR(darray_str_resize(&green->settings, 1 + sz));
    ERR(str_set(darray_str_data_get(&green->settings) + sz, line));
  }
  ERR(darray_result_resize(&green->results,
    darray_str_size_get(&green->settings)));
  txtrdr_ref_put(txtrdr);
  txtrdr = NULL;

exit:
  if(stream) fclose(stream);
  if(txtrdr) txtrdr_ref_put(txtrdr);
  return res;
error:
  goto exit;
}
