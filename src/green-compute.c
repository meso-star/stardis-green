/* Copyright (C) 2020-2022, 2024 |Méso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#define _POSIX_C_SOURCE 200112L /* strtok_r */

#include "green-compute.h"
#include "green-types.h"

#include <rsys/str.h>
#include <rsys/cstr.h>
#include <rsys/logger.h>
#include <rsys/text_reader.h>
#include <rsys/dynamic_array.h>
#include <rsys/mem_allocator.h>

#include <math.h>
#include <string.h>
#include <ctype.h>
#include <omp.h>

enum log_record_type {
  RECORD_INVALID = BIT(0),
  RECORD_UNKNOWN = BIT(1),
  RECORD_UNUSED = BIT(2),
  RECORD_DEFAULT = BIT(3),
  RECORD_TYPES_UNDEF__ = BIT(4)
};

struct log_record {
  enum log_record_type type;
  int line_num, var_num;
  struct str name;
  const char* keep_line;
  double default_value;
  struct mem_allocator* allocator;
};

static FINLINE res_T
log_record_create
  (struct mem_allocator* allocator,
   const enum log_record_type type,
   struct log_record** out_record)
{
  struct log_record* record;
  ASSERT(allocator && out_record);
  record = MEM_ALLOC(allocator, sizeof(*record));
  if(!record) return RES_MEM_ERR;
  record->type = type;
  record->line_num = -1;
  record->var_num = -1;
  record->keep_line = NULL;
  record->default_value = INF;
  str_init(allocator, &record->name);
  record->allocator = allocator;
  *out_record = record;
  return RES_OK;
}

static FINLINE void
log_record_release
  (struct log_record* data)
{
  str_release(&data->name);
  MEM_RM(data->allocator, data);
}

#define DARRAY_NAME logs
#define DARRAY_DATA struct log_record*
#include <rsys/dynamic_array.h>

void
check_green_table_variables_use
  (struct green* green)
{
  size_t i;
  unsigned j;

  ASSERT(green);
  ASSERT(!green->references_checked); /* Fix double call in caller */

  FOR_EACH(i, 0, green->header.ok_count) {
    const struct sample* sample = green->samples + i;
    unsigned id = sample->header.sample_end_description_id;
    /* Ambient ID is description_count */
    ASSERT(id <= green->header.description_count);
    ASSERT(id == green->header.description_count
      || green->descriptions[id].type == GREEN_MAT_SOLID
      || green->descriptions[id].type == GREEN_MAT_FLUID
      || green->descriptions[id].type == GREEN_BOUND_H
      || green->descriptions[id].type == GREEN_BOUND_T);
    if(sample->header.at_initial)
      green->table[id].initial_T_weight++;
    else
      green->table[id].imposed_T_weight++;

    FOR_EACH(j, 0, sample->header.pw_count) {
      const double w = sample->pw_weights[j];
      id = sample->pw_ids[j];
      ASSERT(id < green->header.description_count);
      ASSERT(green->descriptions[id].type == GREEN_MAT_SOLID
        || green->descriptions[id].type == GREEN_MAT_FLUID);
      green->table[id].other_weight += w;
    }
    FOR_EACH(j, 0, sample->header.fx_count) {
      const double w = sample->fx_weights[j];
      id = sample->fx_ids[j];
      ASSERT(id < green->header.description_count);
      ASSERT(green->descriptions[id].type == GREEN_BOUND_F);
      green->table[id].other_weight += w;
    }
  }
  green->references_checked = 1;
}

#define INSERT(Elt, Field) { \
  struct variable_data vd___; \
  vd___.idx = ((Elt) * sizeof(struct applied_settings_elt) \
    + offsetof(struct applied_settings_elt, Field ## _value)) / sizeof(double); \
  vd___.used = (green->table[Elt].Field ## _weight != 0); \
  vd___.unknown = green->table[Elt].Field ## _unknown; \
  if(vd___.unknown) green->unknown_variables = 1; \
  else if(!vd___.used) green->unused_variables = 1; \
  if(htable_variable_ptr_find(&green->variable_ptrs, \
      &(green->table[Elt].Field ## _name))) {\
    /* Should have been detected by stardis: corrupted file??? */ \
    logger_print(green->logger, LOG_ERROR, \
      "Duplicate description name found.\n"); \
    res = RES_BAD_ARG; \
    goto error; \
  } \
  ERR(htable_variable_ptr_set(&green->variable_ptrs, \
    &(green->table[Elt].Field ## _name), &vd___)); \
} (void)0

#define MK_VAR(Str, Desc) \
  for(;;) { \
    size_t n = (size_t)snprintf(buf, bufsz, (Str), (Desc).name);\
    if(n < bufsz) break; \
    buf = MEM_REALLOC(green->allocator, buf, n + 1); \
    bufsz = n + 1; \
  } (void)0

res_T
build_green_table
  (struct green* green)
{
  res_T res = RES_OK;
  size_t i, ambient_id;
  char* buf = NULL;
  size_t bufsz = 32;

  ASSERT(green);

  green->table = MEM_CALLOC(green->allocator, 1+green->header.description_count,
    sizeof(*green->table));
  buf = MEM_ALLOC(green->allocator, bufsz);
  if(!green->table || ! buf) {
    res = RES_MEM_ERR;
    goto error;
  }
  FOR_EACH(i, 0, 1 + green->header.description_count)
    init_table_elt(green->allocator, green->table + i);

  check_green_table_variables_use(green);

  FOR_EACH(i, 0, green->header.description_count) {
    struct green_description* desc = green->descriptions + i;
    switch(desc->type) {
      case GREEN_BOUND_T:
        MK_VAR("%s.T", desc->d.t_boundary);
        ERR(str_set(&green->table[i].imposed_T_name, buf));
        ASSERT(desc->d.t_boundary.imposed_temperature >= 0);
        green->table[i].imposed_T_value = desc->d.t_boundary.imposed_temperature;
        green->table[i].imposed_T_defined = 1;
        INSERT(i, imposed_T);
        break;
      case GREEN_BOUND_H:
        MK_VAR("%s.T", desc->d.h_boundary);
        ERR(str_set(&green->table[i].imposed_T_name, buf));
        ASSERT(desc->d.h_boundary.imposed_temperature >= 0);
        green->table[i].imposed_T_value = desc->d.h_boundary.imposed_temperature;
        green->table[i].imposed_T_defined = 1;
        INSERT(i, imposed_T);
        break;
      case GREEN_BOUND_F:
        MK_VAR("%s.F", desc->d.f_boundary);
        ERR(str_set(&green->table[i].other_name, buf));
        green->table[i].other_value = desc->d.f_boundary.imposed_flux;
        green->table[i].other_defined = 1;
        INSERT(i, other);
        break;
      case GREEN_MAT_SOLID:
        MK_VAR("%s.T", desc->d.solid);
        ERR(str_set(&green->table[i].imposed_T_name, buf));
        green->table[i].imposed_T_value = desc->d.solid.imposed_temperature;
        green->table[i].imposed_T_defined = 1;
        if(green->table[i].imposed_T_value < 0)
          green->table[i].imposed_T_unknown = 1;
        INSERT(i, imposed_T);
        MK_VAR("%s.Tinit", desc->d.solid);
        ERR(str_set(&green->table[i].initial_T_name, buf));
        green->table[i].initial_T_value = desc->d.solid.initial_temperature;
        green->table[i].initial_T_defined = 1;
        if(green->table[i].initial_T_value < 0)
          green->table[i].initial_T_unknown = 1;
        INSERT(i, initial_T);
        MK_VAR("%s.VP", desc->d.solid);
        ERR(str_set(&green->table[i].other_name, buf));
        green->table[i].other_value = desc->d.solid.volumic_power;
        green->table[i].other_defined = 1;
        INSERT(i, other);
        break;
      case GREEN_MAT_FLUID:
        MK_VAR("%s.T", desc->d.fluid);
        ERR(str_set(&green->table[i].imposed_T_name, buf));
        green->table[i].imposed_T_value = desc->d.fluid.imposed_temperature;
        green->table[i].imposed_T_defined = 1;
        if(green->table[i].imposed_T_value < 0)
          green->table[i].imposed_T_unknown = 1;
        INSERT(i, imposed_T);
        MK_VAR("%s.Tinit", desc->d.fluid);
        ERR(str_set(&green->table[i].initial_T_name, buf));
        green->table[i].initial_T_value = desc->d.fluid.initial_temperature;
        green->table[i].initial_T_defined = 1;
        if(green->table[i].initial_T_value < 0)
          green->table[i].initial_T_unknown = 1;
        INSERT(i, initial_T);
        break;
      case GREEN_SOLID_SOLID_CONNECT:
      case GREEN_SOLID_FLUID_CONNECT:
        /* No variables here */
        break;
      default:
        logger_print(green->logger, LOG_ERROR, "Invalid description type.\n");
        res = RES_BAD_ARG;
        goto error;
    }
  }
  /* Ambient ID is description_count */
  ambient_id = green->header.description_count;
  ERR(str_set(&green->table[ambient_id].imposed_T_name, "AMBIENT"));
  green->table[ambient_id].imposed_T_value
    = green->header.ambient_radiative_temperature;
  green->table[ambient_id].imposed_T_defined = 1;
  INSERT(ambient_id, imposed_T);

end:
  MEM_RM(green->allocator, buf);
  return res;
error:
  goto end;
}

#undef INSERT

static void
applied_settings_set_defaults
  (const struct green* green,
   struct applied_settings_elt* settings)
{
  unsigned i;
  FOR_EACH(i, 0, 1 + green->header.description_count) {
    settings[i].imposed_T_value = green->table[i].imposed_T_value;
    settings[i].initial_T_value = green->table[i].initial_T_value;
    settings[i].other_value = green->table[i].other_value;
  }
}

static void
green_compute_1
  (const struct green* green,
   const struct applied_settings_elt* settings,
   double* E,
   double* STD)
{
  size_t i;
  unsigned j;
  double V, Ti, T = 0, T2 = 0;

  ASSERT(green && E && STD);

  FOR_EACH(i, 0, green->header.ok_count) {
    const struct sample* sample = green->samples + i;
    unsigned id = sample->header.sample_end_description_id;
    ASSERT(id <= green->header.description_count); /* Ambient ID is description_count */
    ASSERT(id == green->header.description_count
      || (green->descriptions[id].type == GREEN_BOUND_T)
      || (green->descriptions[id].type == GREEN_BOUND_H)
      || (green->descriptions[id].type == GREEN_MAT_SOLID)
      || (green->descriptions[id].type == GREEN_MAT_FLUID));
    if(sample->header.at_initial) {
      ASSERT(green->table[id].initial_T_defined
        && !green->table[id].initial_T_unknown);
      Ti = settings[id].initial_T_value;
    } else {
      ASSERT(green->table[id].imposed_T_defined
        && !green->table[id].imposed_T_unknown);
      Ti = settings[id].imposed_T_value;
    }

    FOR_EACH(j, 0, sample->header.pw_count) {
      const double w = sample->pw_weights[j];
      id = sample->pw_ids[j];
      ASSERT(id < green->header.description_count);
      ASSERT(green->descriptions[id].type == GREEN_MAT_SOLID
        || green->descriptions[id].type == GREEN_MAT_FLUID);
      ASSERT(green->table[id].other_defined && !green->table[id].other_unknown);
      Ti += w * settings[id].other_value;
    }
    FOR_EACH(j, 0, sample->header.fx_count) {
      const double w = sample->fx_weights[j];
      id = sample->fx_ids[j];
      ASSERT(id < green->header.description_count);
      ASSERT(green->descriptions[id].type == GREEN_BOUND_F);
      ASSERT(green->table[id].other_defined && !green->table[id].other_unknown);
      Ti += w * settings[id].other_value;
    }
    T += Ti;
    T2 += Ti * Ti;
  }
  *E = T / (double)green->header.ok_count;
  V = T2 / (double)green->header.ok_count - *E * *E;
  *STD = (V > 0) ? sqrt(V / (double)green->header.ok_count) : 0;
}

static int
cmp_records
  (void const* r1, void const* r2)
{
  struct log_record* record1;
  struct log_record* record2;
  ASSERT(r1 && r2);
  record1 = *(struct log_record**)r1;
  record2 = *(struct log_record**)r2;
  if(record1->line_num == record2->line_num)
    return record1->var_num - record2->var_num;
  return record1->line_num - record2->line_num;
}

static void
do_log
  (const char* file_name,
   struct green* green,
   struct darray_logs* logs)
{
  size_t i;
  int prev_line_num = INT_MAX;
  enum log_type log_type = LOG_WARNING;
  ASSERT(file_name && green && logs);
  
  if(darray_logs_size_get(logs) == 0) return;

  /* Sort records by #line */
  qsort(darray_logs_data_get(logs),
    darray_logs_size_get(logs),
    sizeof(*darray_logs_data_get(logs)),
    cmp_records);

  FOR_EACH(i, 0, darray_logs_size_get(logs)) {
    struct log_record* record = darray_logs_data_get(logs)[i];
    if(record->type & (RECORD_INVALID | RECORD_UNKNOWN)) {
      log_type = LOG_ERROR;
      break;
    }
  }
  logger_print(green->logger, log_type, "In file '%s':\n", file_name);
  FOR_EACH(i, 0, darray_logs_size_get(logs)) {
    struct log_record* record = darray_logs_data_get(logs)[i];
    switch (record->type) {
      case RECORD_INVALID:
        if(record->line_num != prev_line_num) {
          logger_print(green->logger, LOG_ERROR,
            "In line #%d: '%s':\n",
            record->line_num, record->keep_line);
          logger_print(green->logger, LOG_ERROR,
            (str_is_empty(&record->name)
              ? "Invalid data (missing name)\n"  : "Invalid data (missing value)\n"));
        } else
          logger_print(green->logger, LOG_ERROR,
            (str_is_empty(&record->name)
              ? "Invalid data (missing name)\n" : "Invalid data (missing value)\n"));
        break;
      case RECORD_UNKNOWN:
        if(record->line_num != prev_line_num) {
          logger_print(green->logger, LOG_ERROR,
            "In line #%d: '%s':\n",
            record->line_num, record->keep_line);
          logger_print(green->logger, LOG_ERROR,
            "Unknown variable name: '%s'\n",
            str_cget(&record->name));
        } else
          logger_print(green->logger, LOG_ERROR,
            "\tUnknown variable name: '%s'\n",
            str_cget(&record->name));
        break;
      case RECORD_UNUSED:
        if(record->line_num != prev_line_num) {
          logger_print(green->logger, LOG_WARNING,
            "In line #%d: '%s':\n",
            record->line_num, record->keep_line);
          logger_print(green->logger, LOG_WARNING,
            "Attempt to change unused variable '%s'.\n",
            str_cget(&record->name));
        } else
          logger_print(green->logger, LOG_WARNING,
            "\tAttempt to change unused variable '%s'.\n",
            str_cget(&record->name));
        break;
      case RECORD_DEFAULT:
        if(record->line_num != prev_line_num) {
          logger_print(green->logger, LOG_WARNING,
            "In line #%d: '%s':\n",
            record->line_num, record->keep_line);
          logger_print(green->logger, LOG_WARNING,
            "Change variable '%s' to its default value %g.\n",
            str_cget(&record->name), record->default_value);
        } else
          logger_print(green->logger, LOG_WARNING,
            "Change variable '%s' to its default value %g.\n",
            str_cget(&record->name), record->default_value);
        break;
      default:
        FATAL("error:" STR(__FILE__) ":" STR(__LINE__)": Invalid type.\n");
    }
    prev_line_num = record->line_num;
  }
}

static res_T
parse_line
  (const char* file_name,
   const int idx,
   struct green* green,
   struct applied_settings_elt* settings,
   struct darray_logs* logs)
{
  res_T res = RES_OK;
  int name_count;
  struct str name;
  char* tk_name = NULL;
  char* tk_value = NULL;
  char* tok_ctx = NULL;
  const struct str* keep_line;
  struct str line;
  ASSERT(file_name && green && settings && logs && idx >= 0);
  (void)file_name;

  keep_line = darray_str_cdata_get(&green->settings) + idx;
  str_init(green->allocator, &line);
  str_init(green->allocator, &name);

  ERR(str_set(&line, str_cget(keep_line)));

  /* At least one name=value, no name without value */
  for(name_count = 0; ; name_count++) {
    struct variable_data* vd;
    struct log_record* record = NULL;
    double prev;

    if(res != RES_OK) goto error;

    tk_name = strtok_r(name_count ? NULL : str_get(&line), "=", &tok_ctx);
    if(tk_name) {
      char* c;
      c = tk_name + strlen(tk_name) - 1;
      while(c >= tk_name && isspace(*tk_name)) tk_name++;
      while(c >= tk_name && isspace(*c)) {
        *c = '\0';
        c--;
      }
      if(c < tk_name) tk_name = NULL;
    }
    tk_value = strtok_r(NULL, " \t", &tok_ctx);
    if((tk_name == NULL) != (tk_value == NULL)) {
      /* '<name> = <value>' or '' */
      ERR(log_record_create(green->allocator, RECORD_INVALID, &record));
      if(tk_name) { ERR(str_set(&record->name, tk_name)); }
      res = RES_BAD_ARG;
      goto push_record;
    }
    if(!tk_name) break; /* End of data */
    /* Check name validity */
    ERR(str_set(&name, tk_name));
    vd = htable_variable_ptr_find(&green->variable_ptrs, &name);
    if(!vd) {
      ERR(log_record_create(green->allocator, RECORD_UNKNOWN, &record));
      ERR(str_set(&record->name, tk_name));
      res = RES_BAD_ARG;
      goto push_record;
    }
    if(!vd->used) {
      ERR(log_record_create(green->allocator, RECORD_UNUSED, &record));
      ERR(str_set(&record->name, tk_name));
      goto push_record;
    }
    /* Variable exists and is used in Green function: check if really changed */
    prev = ((double*)settings)[vd->idx];
    ERR(cstr_to_double(tk_value, ((double*)settings) + vd->idx));
    if(prev == ((double*)settings)[vd->idx]) {
      ERR(log_record_create(green->allocator, RECORD_DEFAULT, &record));
      ERR(str_set(&record->name, tk_name));
      record->default_value = prev;
      goto push_record;
    }
    continue;
push_record:
    #pragma omp critical
    {
      res_T tmp_res;
      ASSERT(record && record->type != RECORD_TYPES_UNDEF__);
      record->line_num = idx;
      record->var_num = name_count;
      record->keep_line = str_cget(keep_line);
      tmp_res = darray_logs_push_back(logs, &record);
      if(res == RES_OK) res = tmp_res;
    }
  }

end:
  str_release(&line);
  str_release(&name);
  return res;
error:
  goto end;
}

res_T
green_compute
  (struct green* green,
   const char* in_name)
{
  res_T res = RES_OK;
  size_t sz, n;
  int i;
  struct applied_settings_elt** settings = NULL;
  struct darray_logs logs;

  ASSERT(green && in_name);
  sz = darray_str_size_get(&green->settings);
  ASSERT(sz < INT_MAX);

  settings = MEM_CALLOC(green->allocator, green->nthreads, sizeof(*settings));
  if(settings == NULL) {
    res = RES_MEM_ERR;
    goto error;
  }
  darray_logs_init(green->allocator, &logs);

  omp_set_num_threads((int)green->nthreads);
  #pragma omp parallel for schedule(static)
  for(i = 0; i < (int)sz; i++) {
    struct result* result = darray_result_data_get(&green->results) + i;
    res_T tmp_res = RES_OK;
    int t = omp_get_thread_num();

    if(res != RES_OK) continue;

    if(settings[t] == NULL) {
      /* description_count + 1 for AMBIENT */
      settings[t] = MEM_CALLOC(green->allocator, 1 + green->header.description_count,
        sizeof(**settings));
      if(settings[t] == NULL) {
        res = RES_MEM_ERR;
        continue;
      }
    }

    /* Apply settings */
    applied_settings_set_defaults(green, settings[t]);
    tmp_res = parse_line(in_name, i, green, settings[t], &logs);
    if(tmp_res != RES_OK) {
      res = tmp_res;
      continue;
    }
    /* Compute */
    green_compute_1(green, settings[t], &result->E, &result->STD);
  } /* Implicit barrier */
  if(res != RES_OK) goto error;

exit:
  do_log(in_name, green, &logs);
  if(settings) {
    FOR_EACH(n, 0, green->nthreads) MEM_RM(green->allocator, settings[n]);
    MEM_RM(green->allocator, settings);
  }
  FOR_EACH(n, 0, darray_logs_size_get(&logs)) {
    struct log_record* record = darray_logs_data_get(&logs)[n];
    log_record_release(record);
  }
  darray_logs_release(&logs);
  return res;
error:
  goto exit;
}
