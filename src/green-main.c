/* Copyright (C) 2020-2022, 2024 |Méso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "green-types.h"
#include "green-args.h"
#include "green-output.h"
#include "green-compute.h"

#include <rsys/rsys.h>
#include <rsys/mem_allocator.h>
#include <rsys/logger.h>
#include <rsys/clock_time.h>

#include <stdlib.h>
#include <omp.h>

int
main
  (int argc,
   char** argv)
{
  int err = EXIT_SUCCESS;
  res_T res = RES_OK;
  int logger_initialized = 0, green_initialized = 0, allocator_initialized = 0;
  struct mem_allocator allocator;
  struct logger logger;
  struct green green;
  struct args args;
  struct time init_start, tmp;
  char buf[128];
  const int flag = TIME_USEC | TIME_MSEC | TIME_SEC | TIME_MIN | TIME_HOUR ;
  FILE* stream = NULL;

  time_current(&init_start);

  ERR(mem_init_proxy_allocator(&allocator, &mem_default_allocator));
  allocator_initialized = 1;

  ERR(logger_init(&allocator, &logger));
  logger_initialized = 1;
  /* Active loggin for args pasing */
  logger_set_stream(&logger, LOG_OUTPUT, log_prt, NULL);
  logger_set_stream(&logger, LOG_ERROR, log_err, NULL);
  logger_set_stream(&logger, LOG_WARNING, log_warn, NULL);

  init_args(&logger, &allocator, &args);
  ERR(parse_args(argc, argv, &args));

  if(args.mode & MODE_HELP) {
    usage(stdout);
    goto exit;
  }

  if(args.mode & MODE_VERSION) {
    print_version(stdout);
    goto exit;
  }
  /* Deactivate some loggin according to the -V arg */
  if(args.verbose < 1)
    logger_set_stream(&logger, LOG_ERROR, NULL, NULL);
  if(args.verbose < 2)
    logger_set_stream(&logger, LOG_WARNING, NULL, NULL);
  if(args.verbose < 3)
    logger_set_stream(&logger, LOG_OUTPUT, NULL, NULL);

  green_init(&allocator, &logger, args.nthreads, &green);
  green_initialized = 1;
  #pragma omp parallel sections num_threads(args.nthreads)
  {
    /* If there are some settings to apply read the 2 files at the same time*/
    #pragma omp section
    if(args.green_filename) {
      stream = fopen(args.green_filename, "rb");
      if(!stream) {
        logger_print(&logger, LOG_ERROR,
          "Cannot open model file '%s'\n", args.green_filename);
        res = RES_IO_ERR;
      } else {
        res_T tmp_res;
        tmp_res = read_green_function(&green, stream);
        fclose(stream); stream = NULL;
        if(tmp_res == RES_MEM_ERR) {
          logger_print(&logger, LOG_ERROR, "Could not allocate memory.\n");
          res = tmp_res;
        }
        else if(tmp_res != RES_OK) {
          logger_print(&logger, LOG_ERROR,
            "Cannot read model file '%s' (file could be corrupted).\n",
            args.green_filename);
          res = tmp_res;
        }
      }
    }
    #pragma omp section
    if(args.mode & MODE_APPLY_GREEN) {
      res_T tmp_res;
      tmp_res = green_read_settings(&green, args.command_filename);
      if(tmp_res == RES_MEM_ERR) {
        logger_print(&logger, LOG_ERROR, "Could not allocate memory.\n");
        res = tmp_res;
      }
      else if(tmp_res != RES_OK) {
        logger_print(&logger, LOG_ERROR,
          "Cannot read settings file '%s' (file could be corrupted).\n",
          args.green_filename);
        res = tmp_res;
      }
    }
  } /* Implicit barrier */
  if(res != RES_OK) goto error;
  if(args.mode & MODE_EXTENTED_RESULTS) {
    struct time init_end;
    time_current(&init_end);
    time_sub(&tmp, &init_end, &init_start);
    time_dump(&tmp, flag, NULL, buf, sizeof(buf));
    logger_print(&logger, LOG_OUTPUT, "Initialisation time = %s\n", buf);
  }

  if(args.mode & MODE_HTML_SUMMARY) {
    ASSERT(args.info_filename);
    stream = fopen(args.info_filename, "w");
    if(!stream) {
      logger_print(&logger, LOG_ERROR,
        "Cannot open model file '%s'\n", args.info_filename);
      res = RES_IO_ERR;
      goto error;
    }
    dump_green_info(&green, stream);
    fclose(stream); stream = NULL;
  }

  if(args.mode & MODE_APPLY_GREEN) {
    struct time compute_start, compute_end;
    time_current(&compute_start);
    ERR(green_compute(&green, args.command_filename));
    if(args.mode & MODE_EXTENTED_RESULTS) {
      time_current(&compute_end);
      time_sub(&tmp, &compute_end, &compute_start);
      time_dump(&tmp, flag, NULL, buf, sizeof(buf));
      logger_print(&logger, LOG_OUTPUT, "Computation time = %s\n", buf);
    }
    green_print_result(&green, args.mode, stdout);
  }

exit:
  if(green_initialized) green_release(&green);
  if(logger_initialized) logger_release(&logger);
  if(stream) fclose(stream);
  if(allocator_initialized) {
    if(MEM_ALLOCATED_SIZE(&allocator) != 0) {
      char dump[4096] = { '\0' };
      MEM_DUMP(&allocator, dump, sizeof(dump));
      fprintf(stderr, "%s\n", dump);
      fprintf(stderr, "\nMemory leaks: %lu Bytes\n",
        (unsigned long)MEM_ALLOCATED_SIZE(&allocator));
    }
    mem_shutdown_proxy_allocator(&allocator);
  }
  return err;
error:
  err = EXIT_FAILURE;
  goto exit;
}
