/* Copyright (C) 2020-2022, 2024 |Méso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "green-args.h"
#include "green-default.h"
#include "green-types.h"

#include <rsys/rsys.h>
#include <rsys/logger.h>
#include <rsys/cstr.h>
#include <rsys/str.h>

#include <getopt.h>

static char
mode_option
  (const int m)
{
  int found = 0;
  char res = '?';
  if(m & MODE_APPLY_GREEN) { found++; res = 'a'; }
  if(m & MODE_EXTENTED_RESULTS) { found++; res = 'e'; }
  if(m & MODE_READ_GREEN) { found++; res = 'g'; }
  if(m & MODE_HELP) { found++; res = 'h'; }
  if(m & MODE_HTML_SUMMARY) { found++; res = 's'; }
  if(m & MODE_THREADS_COUNT) { found++; res = 't'; }
  if(m & MODE_VERSION) { found++; res = 'v'; }
  if(m & MODE_VERBOSE) { found++; res = 'V'; }
  ASSERT(found == 1);
  return res;
}

void
init_args
  (struct logger* logger,
   struct mem_allocator* allocator,
   struct args* args)
{
  ASSERT(logger && allocator && args);
  args->logger = logger;
  args->allocator = allocator;
  /* Set default values */
  args->mode = MODE_NULL;
  args->command_filename = NULL;
  args->green_filename = NULL;
  args->info_filename = NULL;
  args->nthreads = (unsigned)omp_get_num_procs();
  args->verbose = GREEN_DEFAULT_VERBOSE_LEVEL;
}

res_T
parse_args
  (const int argc,
   char** argv,
   struct args* args)
{
  int opt = 0;
  const char option_list[] = "a:ehg:s:t:vV:";
  res_T res = RES_OK;

  ASSERT(argv && args);

  opterr = 0; /* No default error messages */
  while ((opt = getopt(argc, argv, option_list)) != -1) {
    switch (opt) {

    case '?': /* Unreconised option */
    {
      char* ptr = strchr(option_list, optopt);
      if(ptr && ptr[1] == ':') {
        res = RES_BAD_ARG;
        logger_print(args->logger, LOG_ERROR,
          "Missing argument for option -%c\n",
          optopt);
      } else {
        logger_print(args->logger, LOG_ERROR, "Invalid option -%c\n", optopt);
      }
      goto error;
    }

    case 'a':
      args->command_filename = optarg;
      args->mode |= MODE_APPLY_GREEN;
      break;

    case 'e':
      args->mode |= MODE_EXTENTED_RESULTS;
      break;

    case 'h':
      args->mode |= MODE_HELP;
      break;

    case 'g':
      if(args->green_filename) {
        res = RES_BAD_ARG;
        logger_print(args->logger, LOG_ERROR,
          "Option -%c cannot be used twice.\n",
          opt);
        goto error;
      }
      args->mode |= MODE_READ_GREEN;
      args->green_filename = optarg;
      break;

    case 's':
      args->info_filename = optarg;
      args->mode |= MODE_HTML_SUMMARY;
      break;

    case 't':
      res = cstr_to_uint(optarg, &args->nthreads);
      if(res != RES_OK
        || args->nthreads <= 0)
      {
        if(res == RES_OK) res = RES_BAD_ARG;
        logger_print(args->logger, LOG_ERROR,
          "Invalid argument for option -%c: %s\n",
          opt, optarg);
        goto error;
      }
      args->mode |= MODE_THREADS_COUNT;
      break;

    case 'v':
      args->mode |= MODE_VERSION;
      break;

    case 'V':
      res = cstr_to_int(optarg, &args->verbose);
      if(res != RES_OK
        || args->verbose < 0
        || args->verbose > 3)
      {
        if(res == RES_OK) res = RES_BAD_ARG;
        logger_print(args->logger, LOG_ERROR,
          "Invalid argument for option -%c: %s\n",
          opt, optarg);
        goto error;
      }
      args->mode |= MODE_VERBOSE;
      break;
    }
  }

  if(args->mode == MODE_NULL) {
    res = RES_BAD_ARG;
    goto error;
  }

  if(!(args->mode & MODE_READ_GREEN)) {
    if((args->mode & MODE_HTML_SUMMARY)) {
      res = RES_BAD_ARG;
      logger_print(args->logger, LOG_ERROR,
        "Option -%c can only be used in conjunction with option -%c.\n",
        mode_option(MODE_HTML_SUMMARY), mode_option(MODE_READ_GREEN));
      goto error;
    }
    if((args->mode & MODE_APPLY_GREEN)) {
      res = RES_BAD_ARG;
      logger_print(args->logger, LOG_ERROR,
        "Option -%c can only be used in conjunction with option -%c.\n",
        mode_option(MODE_APPLY_GREEN), mode_option(MODE_READ_GREEN));
      goto error;
    }
  }
  if((args->mode & MODE_EXTENTED_RESULTS
    && !(args->mode & MODE_APPLY_GREEN))) {
    res = RES_BAD_ARG;
    logger_print(args->logger, LOG_ERROR,
      "Option -%c can only be used in conjunction with option -%c.\n",
      mode_option(MODE_EXTENTED_RESULTS), mode_option(MODE_APPLY_GREEN));
    goto error;
  }

  end:
    return res;
  error:
    logger_print(args->logger, LOG_ERROR, "Use option -h to print help.\n");
    goto end;
  }
