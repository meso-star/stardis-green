/* Copyright (C) 2020-2022, 2024 |Méso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef GREEN_COMPUTE_H
#define GREEN_COMPUTE_H

#include <rsys/rsys.h>

#include <stdio.h>

struct green;

void
check_green_table_variables_use
  (struct green* green);

res_T
build_green_table
  (struct green* green);

res_T
green_compute
  (struct green* green,
   const char* in_name);

#endif /* GREEN_COMPUTE_H */