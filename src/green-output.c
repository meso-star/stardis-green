/* Copyright (C) 2020-2022, 2024 |Méso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include <rsys/rsys.h>
#include <rsys/str.h>
#include <rsys/mem_allocator.h>

#include "green-output.h"
#include "green-types.h"
#include "green-version.h"

#include <stdio.h>

#define CELL(T,Val) \
  fprintf(stream, "  <td>%" #T "</td>\n", (Val))

#define VAR_CELL(Elt, Field) { \
  const char* name___ = str_cget(&(green->table[Elt].Field ## _name)); \
  if((green->table[Elt]). Field ## _unknown) { \
    fprintf(stream, "  <td>%s = Unknown</td>\n", name___); \
  } else if(green->table[Elt].Field ## _defined \
      && green->table[Elt].Field ## _weight > 0) \
  { \
    fprintf(stream, "  <td><u><b>%s = %g</b></u></td>\n", \
      name___, green->table[Elt].Field ## _value); \
  } else { \
    fprintf(stream, "  <td>%s = %g</td>\n", \
      name___, green->table[Elt].Field ## _value); \
  } \
} (void)0

void
print_version
  (FILE* stream)
{
  ASSERT(stream);
  fprintf(stream,
    "Green version %i.%i.%i\n",
    GREEN_VERSION_MAJOR, GREEN_VERSION_MINOR, GREEN_VERSION_PATCH);
}

void
usage(FILE * stream)
{

  fprintf(stream,
    "sgreen [-ehv] [-a arguments] [-g green] [-s summary] [-t threads_count]\n"
    "       [-V verbosity_level]\n");
}

void
green_print_result
  (struct green* green,
   const int mode,
   FILE* out_stream)
{
  size_t i, sz;

  ASSERT(green && out_stream);
  sz = darray_str_size_get(&green->settings);

  for(i = 0; i < sz; i++) {
    struct result* result = darray_result_data_get(&green->results) + i;
    const char* line = str_cget(darray_str_data_get(&green->settings) + i);
    if(mode & MODE_EXTENTED_RESULTS)
      fprintf(out_stream, "%g K +/- %g ; %s\n", result->E, result->STD, line);
    else fprintf(out_stream, "%g %g\n", result->E, result->STD);
  }
}

void
dump_green_info
  (struct green* green,
   FILE* stream)
{
  size_t i, local_count;
  double t = 0;
  int fst = 1;

  ASSERT(green && stream);
  ASSERT(green->references_checked);

  /* Output html headers */
  fprintf(stream, "<!DOCTYPE html>\n");
  fprintf(stream, "<html>\n");
  fprintf(stream, "<head>\n");
  fprintf(stream, "<title>Green function description</title>\n");

  fprintf(stream, "<style>\n");
  fprintf(stream, "  table, th, td{\n");
  fprintf(stream, "    border: 1px solid black;\n");
  fprintf(stream, "    border-collapse: collapse;\n");
  fprintf(stream, "  }\n");
  fprintf(stream, "  th, td{\n");
  fprintf(stream, "    padding: 5px \n");
  fprintf(stream, "  }\n");
  fprintf(stream, "  td{\n");
  fprintf(stream, "    text-align: right;\n");
  fprintf(stream, "  }\n");
  fprintf(stream, "</style>\n");

  fprintf(stream, "</head>\n");
  fprintf(stream, "<body>\n");

  fprintf(stream, "<h1>Green function description</h1>\n");

  fprintf(stream, "<h2>List of variables</h2>\n");

  fprintf(stream, "<h3>Variables used in the Green function</h3>\n");

  fprintf(stream, "<p>These variable names are used in the following formula. "
    "Using the syntax NAME=value in a settings file, their values are "
    "modifiable when applying the Green Function. They are listed <u><b>NAME "
    "= value</b></u> in the tables below.</p>");

  fst = 1;
  FOR_EACH(i, 0, green->header.description_count + 1) {
    struct table_elt* elt = green->table + i;
    if(elt->imposed_T_defined && elt->imposed_T_weight > 0) {
      if(!fst) fprintf(stream, ", ");
      fst = 0;
      fprintf(stream, "%s", str_cget(&elt->imposed_T_name));
    }
    if(elt->initial_T_defined && elt->initial_T_weight > 0) {
      if(!fst) fprintf(stream, ", ");
      fst = 0;
      fprintf(stream, "%s", str_cget(&elt->initial_T_name));
    }
    if(elt->other_defined && elt->other_weight) {
      if(!fst) fprintf(stream, ", ");
      fst = 0;
      fprintf(stream, "%s", str_cget(&elt->other_name));
    }
  }

  if(green->unused_variables) {
    fprintf(stream,
      "<h3>Unused variables in the Green function</h3>\n");

    fprintf(stream, "<p>These variable names are not used in the following "
      "formula, just because they where not sampled. As a consequence, "
      "changing their values in a settings file has no effect on the applied "
      "Green function results. They are listed NAME = value in the tables "
      "below.</p>\n");

    fst = 1;
    FOR_EACH(i, 0, green->header.description_count + 1) {
      struct table_elt* elt = green->table + i;
      if(elt->imposed_T_defined
        && !elt->imposed_T_unknown && elt->imposed_T_weight == 0) {
        if(!fst) fprintf(stream, ", ");
        fst = 0;
        fprintf(stream, "%s", str_cget(&elt->imposed_T_name));
      }
      if(elt->initial_T_defined
        && !elt->initial_T_unknown && elt->initial_T_weight == 0) {
        if(!fst) fprintf(stream, ", ");
        fst = 0;
        fprintf(stream, "%s", str_cget(&elt->initial_T_name));
      }
      if(elt->other_defined
        && !elt->other_unknown && !elt->other_weight) {
        if(!fst) fprintf(stream, ", ");
        fst = 0;
        fprintf(stream, "%s", str_cget(&elt->other_name));
      }
    }
  }

  if(green->unknown_variables) {
    fprintf(stream,
      "<h3>Unknown variables in the Green function</h3>\n");

    fprintf(stream, "<p>These names are not true variable names because their "
      "values where defined as <i>unknown</i> in the system, meaning they "
      "required to be solved. Attempting to change their values in a settings "
      "file is an error. They are listed NAME = Unknown in the tables below."
      "</p>\n");

    fst = 1;
    FOR_EACH(i, 0, green->header.description_count + 1) {
      struct table_elt* elt = green->table + i;
      if(elt->imposed_T_unknown) {
        if(!fst) fprintf(stream, ", ");
        fst = 0;
        fprintf(stream, "%s", str_cget(&elt->imposed_T_name));
      }
      if(elt->initial_T_unknown) {
        if(!fst) fprintf(stream, ", ");
        fst = 0;
        fprintf(stream, "%s", str_cget(&elt->initial_T_name));
      }
      if(elt->other_defined && !elt->other_weight) {
        if(!fst) fprintf(stream, ", ");
        fst = 0;
        fprintf(stream, "%s", str_cget(&elt->other_name));
      }
    }
  }

  fprintf(stream, "<h2>Formula of the Monte-Carlo estimate</h2>\n");

  if(green->header.time_range[0] == green->header.time_range[1])
    fprintf(stream, "<p>The Monte-Carlo estimate of the Green function of the "
      "system at t=%g is as follows:</p>\n",
      green->header.time_range[0]);
  else
    fprintf(stream, "<p>The Monte-Carlo estimate of the Green function of the "
      "system with t in [%g %g] is as follows:</p>\n",
      SPLIT2(green->header.time_range));

  /* Print result */
  fst = 1;
  FOR_EACH(i, 0, 1 + green->header.description_count) {
    if(green->table[i].imposed_T_weight > 0) {
      double k;
      CHK(green->table[i].imposed_T_defined
        && green->table[i].imposed_T_value >= 0);
      if(fst) fprintf(stream, "<p>E[T] = ");
      else fprintf(stream, " + ");
      fst = 0;
      k = (double)green->table[i].imposed_T_weight
        / (double)green->header.ok_count;
      t += k * green->table[i].imposed_T_value;
      fprintf(stream, "%g * <b>%s</b>",
        k, str_cget(&green->table[i].imposed_T_name));
    }
    if(green->table[i].initial_T_weight > 0) {
      double k;
      CHK(green->table[i].initial_T_defined
        && green->table[i].initial_T_value >= 0);
      if(fst) fprintf(stream, "<p>E[T] = ");
      else fprintf(stream, " + ");
      fst = 0;
      k = (double)green->table[i].initial_T_weight
        / (double)green->header.ok_count;
      t += k * green->table[i].initial_T_value;
      fprintf(stream, "%g * <b>%s</b>",
        k, str_cget(&green->table[i].initial_T_name));
    }
  }
  ASSERT(!fst);
  FOR_EACH(i, 0, green->header.description_count) {
    if(green->table[i].other_weight) {
      double k;
      CHK(green->table[i].other_defined);
      k = (double)green->table[i].other_weight
        / (double)green->header.ok_count;
      t += k * green->table[i].other_value;
      fprintf(stream, " + %g * <b>%s</b>",
        k, str_cget(&green->table[i].other_name));
    }
  }
  fprintf(stream, " = %g</p>", t);

  /* Counts table */
  fprintf(stream, "<h2>Counts</h2>\n");
  fprintf(stream, "<table>\n");
  /* fprintf(stream, "<table style=\"width:100%%\">\n"); */
  fprintf(stream, "<tr>\n");
  fprintf(stream, "  <th>Solids</th>\n");
  fprintf(stream, "  <th>Fluids</th>\n");
  fprintf(stream, "  <th>T Boundaries</th>\n");
  fprintf(stream, "  <th>H Boundaries</th>\n");
  fprintf(stream, "  <th>F Boundaries</th>\n");
  fprintf(stream, "  <th>Solid-Fluid Connections</th>\n");
  fprintf(stream, "  <th>Solid-Solid Connections</th>\n");
  fprintf(stream, "  <th>OK Samples</th>\n");
  fprintf(stream, "  <th>Failures</th>\n");
  fprintf(stream, "</tr>\n");
  fprintf(stream, "<tr>\n");
  CELL(u, green->header.solid_count);
  CELL(u, green->header.fluid_count);
  CELL(u, green->header.tbound_count);
  CELL(u, green->header.hbound_count);
  CELL(u, green->header.fbound_count);
  CELL(u, green->header.sfconnect_count);
  CELL(u, green->header.ssconnect_count);
  CELL(lu, (long unsigned)green->header.ok_count);
  CELL(lu, (long unsigned)green->header.failed_count);
  fprintf(stream, "</tr>\n");
  fprintf(stream, "</table>\n");

  /* Solids table */
  local_count = 0;
  FOR_EACH(i, 0, green->header.description_count) {
    const struct green_description* desc = green->descriptions + i;
    const struct green_solid* sl;
    if(desc->type != GREEN_MAT_SOLID) continue;
    if(!local_count) {
      fprintf(stream, "<h2>Solids</h2>\n");
      fprintf(stream, "<table>\n");
      fprintf(stream, "<tr>\n");
      fprintf(stream, "  <th>Name</th>\n");
      fprintf(stream, "  <th>Conductivity</th>\n");
      fprintf(stream, "  <th>Volumic mass</th>\n");
      fprintf(stream, "  <th>Calorific capacity</th>\n");
      fprintf(stream, "  <th>Volumic Power</th>\n");
      fprintf(stream, "  <th>Imposed temperature</th>\n");
      fprintf(stream, "  <th>Initial temperature</th>\n");
      fprintf(stream, "</tr>\n");
    }
    sl = &desc->d.solid;
    fprintf(stream, "<tr>\n");
    CELL(s, sl->name);
    CELL(g, sl->conductivity);
    CELL(g, sl->volumic_mass);
    CELL(g, sl->calorific_capacity);
    VAR_CELL(i, other);
    VAR_CELL(i, imposed_T);
    VAR_CELL(i, initial_T);
    fprintf(stream, "</tr>\n");
    local_count++;
  }
  if(local_count) fprintf(stream, "</table>\n");

  /* Fluids table */
  local_count = 0;
  FOR_EACH(i, 0, green->header.description_count) {
    const struct green_description* desc = green->descriptions + i;
    const struct green_fluid* fl;
    if(desc->type != GREEN_MAT_FLUID) continue;
    if(!local_count) {
      fprintf(stream, "<h2>Fluids</h2>\n");
      fprintf(stream, "<table>\n");
      fprintf(stream, "<tr>\n");
      fprintf(stream, "  <th>Name</th>\n");
      fprintf(stream, "  <th>Volumic mass</th>\n");
      fprintf(stream, "  <th>Calorific capacity</th>\n");
      fprintf(stream, "  <th>Imposed temperature</th>\n");
      fprintf(stream, "  <th>Initial temperature</th>\n");
      fprintf(stream, "</tr>\n");
    }
    fl = &desc->d.fluid;
    fprintf(stream, "<tr>\n");
    CELL(s, fl->name);
    CELL(g, fl->volumic_mass);
    CELL(g, fl->calorific_capacity);
    VAR_CELL(i, imposed_T);
    VAR_CELL(i, initial_T);
    fprintf(stream, "</tr>\n");
    local_count++;
  }
  if(local_count) fprintf(stream, "</table>\n");

  /* T boundaries tables */
  local_count = 0;
  FOR_EACH(i, 0, green->header.description_count) {
    const struct green_description* desc = green->descriptions + i;
    const struct green_t_boundary* bd;
    if(desc->type != GREEN_BOUND_T) continue;
    if(!local_count) {
      fprintf(stream, "<h2>T boundaries for solids</h2>\n");
      fprintf(stream, "<table>\n");
      fprintf(stream, "<tr>\n");
      fprintf(stream, "  <th>Name</th>\n");
      fprintf(stream, "  <th>Temperature</th>\n");
      fprintf(stream, "</tr>\n");
    }
    bd = &desc->d.t_boundary;
    fprintf(stream, "<tr>\n");
    CELL(s, bd->name);
    ASSERT(green->table[i].imposed_T_defined);
    VAR_CELL(i, imposed_T);
    fprintf(stream, "</tr>\n");
    local_count++;
  }
  if(local_count) fprintf(stream, "</table>\n");

  /* H boundaries tables */
  local_count = 0;
  FOR_EACH(i, 0, green->header.description_count) {
    const struct green_description* desc = green->descriptions + i;
    const struct green_h_boundary* bd;
    if(desc->type != GREEN_BOUND_H) continue;
    if(!local_count) {
      fprintf(stream, "<h2>H boundaries for solids</h2>\n");
      fprintf(stream, "<table>\n");
      fprintf(stream, "<tr>\n");
      fprintf(stream, "  <th>Name</th>\n");
      fprintf(stream, "  <th>Emissivity</th>\n");
      fprintf(stream, "  <th>Specular Fraction</th>\n");
      fprintf(stream, "  <th>Convection coefficient</th>\n");
      fprintf(stream, "  <th>Temperature</th>\n");
      fprintf(stream, "</tr>\n");
    }
    bd = &desc->d.h_boundary;
    fprintf(stream, "<tr>\n");
    CELL(s, bd->name);
    CELL(g, bd->emissivity);
    CELL(g, bd->specular_fraction);
    CELL(g, bd->convection_coefficient);
    ASSERT(green->table[i].imposed_T_defined);
    VAR_CELL(i, imposed_T);
    fprintf(stream, "</tr>\n");
    local_count++;
  }
  if(local_count) fprintf(stream, "</table>\n");

  local_count = 0;
  FOR_EACH(i, 0, green->header.description_count) {
    const struct green_description* desc = green->descriptions + i;
    const struct green_h_boundary* bd;
    if(desc->type != GREEN_BOUND_H) continue;
    if(!local_count) {
      fprintf(stream, "<h2>H boundaries for fluids</h2>\n");
      fprintf(stream, "<table>\n");
      fprintf(stream, "<tr>\n");
      fprintf(stream, "  <th>Name</th>\n");
      fprintf(stream, "  <th>Emissivity</th>\n");
      fprintf(stream, "  <th>Specular Fraction</th>\n");
      fprintf(stream, "  <th>Convection coefficient</th>\n");
      fprintf(stream, "  <th>Temperature</th>\n");
      fprintf(stream, "</tr>\n");
    }
    bd = &desc->d.h_boundary;
    fprintf(stream, "<tr>\n");
    CELL(s, bd->name);
    CELL(g, bd->emissivity);
    CELL(g, bd->specular_fraction);
    CELL(g, bd->convection_coefficient);
    ASSERT(green->table[i].imposed_T_defined);
    VAR_CELL(i, imposed_T);
    fprintf(stream, "</tr>\n");
    local_count++;
  }
  if(local_count) fprintf(stream, "</table>\n");

  /* F boundaries table */
  local_count = 0;
  FOR_EACH(i, 0, green->header.description_count) {
    const struct green_description* desc = green->descriptions + i;
    const struct green_f_boundary* bd;
    if(desc->type != GREEN_BOUND_F) continue;
    if(!local_count) {
      fprintf(stream, "<h2>F boundaries for solids</h2>\n");
      fprintf(stream, "<table>\n");
      fprintf(stream, "<tr>\n");
      fprintf(stream, "  <th>Name</th>\n");
      fprintf(stream, "  <th>Flux</th>\n");
      fprintf(stream, "</tr>\n");
    }
    bd = &desc->d.f_boundary;
    fprintf(stream, "<tr>\n");
    CELL(s, bd->name);
    ASSERT(green->table[i].other_defined);
    VAR_CELL(i, other);
    fprintf(stream, "</tr>\n");
    local_count++;
  }
  if(local_count) fprintf(stream, "</table>\n");

  /* S-F connection table */
  local_count = 0;
  FOR_EACH(i, 0, green->header.description_count) {
    const struct green_description* desc = green->descriptions + i;
    const struct green_solid_fluid_connect* sf;
    if(desc->type != GREEN_SOLID_FLUID_CONNECT) continue;
    if(!local_count) {
      fprintf(stream, "<h2>Solid-Fluid connections</h2>\n");
      fprintf(stream, "<table>\n");
      fprintf(stream, "<tr>\n");
      fprintf(stream, "  <th>Name</th>\n");
      fprintf(stream, "  <th>Emissivity</th>\n");
      fprintf(stream, "  <th>Specular Fraction</th>\n");
      fprintf(stream, "  <th>Convection coefficient</th>\n");
      fprintf(stream, "</tr>\n");
    }
    sf = &desc->d.sf_connect;
    fprintf(stream, "<tr>\n");
    CELL(s, sf->name);
    CELL(g, sf->emissivity);
    CELL(g, sf->specular_fraction);
    CELL(g, sf->convection_coefficient);
    fprintf(stream, "</tr>\n");
    local_count++;
  }
  if(local_count) fprintf(stream, "</table>\n");

  /* S-S connection table */
  local_count = 0;
  FOR_EACH(i, 0, green->header.description_count) {
    const struct green_description* desc = green->descriptions + i;
    const struct green_solid_solid_connect* ss;
    if(desc->type != GREEN_SOLID_SOLID_CONNECT) continue;
    if(!local_count) {
      fprintf(stream, "<h2>Solid-Solid connections</h2>\n");
      fprintf(stream, "<table>\n");
      fprintf(stream, "<tr>\n");
      fprintf(stream, "  <th>Name</th>\n");
      fprintf(stream, "  <th>Thermal Contact Resistance</th>\n");
      fprintf(stream, "</tr>\n");
    }
    ss = &desc->d.ss_connect;
    fprintf(stream, "<tr>\n");
    CELL(s, ss->name);
    CELL(g, ss->thermal_contact_resistance);
    fprintf(stream, "</tr>\n");
    local_count++;
  }
  if(local_count) fprintf(stream, "</table>\n");

  /* Radiative temperatures table */
  fprintf(stream, "<h2>Radiative temperatures</h2>\n");
  fprintf(stream, "<table>\n");
  fprintf(stream, "<tr>\n");
  fprintf(stream, "  <th>Name</th>\n");
  fprintf(stream, "  <th>Temperature</th>\n");
  fprintf(stream, "</tr>\n");
  fprintf(stream, "<tr>\n");
  CELL(s, "Ambient");
  ASSERT(green->table[green->header.description_count].imposed_T_defined);
  VAR_CELL(green->header.description_count, imposed_T);
  fprintf(stream, "</tr>\n");
  fprintf(stream, "<tr>\n");
  CELL(s, "Linearization");
  CELL(g, green->header.ambient_radiative_temperature_reference);
  fprintf(stream, "</tr>\n");
  fprintf(stream, "</table>\n");

  /* Output html footers */
  fprintf(stream, "</body>\n");
  fprintf(stream, "</html>\n");
}

#undef CELL
#undef VAR_CELL
