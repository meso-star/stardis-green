# Copyright (C) 2020-2022, 2024 |Méso|Star> (contact@meso-star.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>. */

.POSIX:
.SUFFIXES: # Clean up default inference rules

include config.mk

default: build_executable

################################################################################
# Program building
################################################################################
SRC =\
 src/green-args.c\
 src/green-compute.c\
 src/green-input.c\
 src/green-main.c\
 src/green-output.c
OBJ = $(SRC:.c=.o)
DEP = $(SRC:.c=.d)

# Headers to configure
HDR =\
 src/green-default.h\
 src/green-version.h

build_executable: .config $(HDR) $(DEP)
	@$(MAKE) -fMakefile $$(for i in $(DEP); do echo -f $${i}; done) sgreen

$(DEP) $(HDR) $(OBJ): config.mk

sgreen: $(OBJ)
	$(CC) $(CFLAGS) $(DPDC_CFLAGS) -o $@ $(OBJ) $(LDFLAGS) $(DPDC_LIBS)

.config: config.mk
	@if ! $(PKG_CONFIG) --atleast-version $(RSYS_VERSION) rsys; then \
	  echo "rsys $(RSYS_VERSION) not found" >&2; exit 1; fi
	@if ! $(PKG_CONFIG) --atleast-version $(STARDIS_VERSION) stardis; then \
	  echo "stardis $(STARDIS_VERSION) not found" >&2; exit 1; fi
	@echo "config done" > $@

src/green-default.h: src/green-default.h.in
	sed -e 's#@GREEN_ARGS_DEFAULT_VERBOSE_LEVEL@#$(GREEN_ARGS_DEFAULT_VERBOSE_LEVEL)#g'\
	    $@.in > $@

src/green-version.h: src/green-version.h.in
	sed -e 's/@GREEN_VERSION_MAJOR@/$(VERSION_MAJOR)/' \
	    -e 's/@GREEN_VERSION_MINOR@/$(VERSION_MINOR)/' \
	    -e 's/@GREEN_VERSION_PATCH@/$(VERSION_PATCH)/' \
	    $@.in > $@

.SUFFIXES: .c .d .o
.c.d:
	@$(CC) $(CFLAGS) $(DPDC_CFLAGS) -MM -MT "$(@:.d=.o) $@" $< -MF $@

.c.o:
	$(CC) $(CFLAGS) $(DPDC_CFLAGS) -c $< -o $@

################################################################################
# Manual pages
################################################################################
man: doc/sgreen.1

doc/sgreen.1: doc/sgreen.1.in
	sed -e 's#@GREEN_ARGS_DEFAULT_VERBOSE_LEVEL@#$(GREEN_ARGS_DEFAULT_VERBOSE_LEVEL)#g'\
	    $@.in > $@

################################################################################
# Installation
################################################################################
install: build_executable man
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/bin" sgreen
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/share/man/man1" doc/sgreen.1
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/share/man/man5" doc/sgreen-input.5
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/share/man/man5" doc/sgreen-output.5
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/share/doc/stardis-green" COPYING
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/share/doc/stardis-green" README.md

uninstall:
	rm -f "$(DESTDIR)$(PREFIX)/bin/sgreen"
	rm -f "$(DESTDIR)$(PREFIX)/share/man/man1/sgreen.1"
	rm -f "$(DESTDIR)$(PREFIX)/share/man/man5/sgreen-input.5"
	rm -f "$(DESTDIR)$(PREFIX)/share/man/man5/sgreen-output.5"
	rm -f "$(DESTDIR)$(PREFIX)/share/doc/stardis-green/COPYING"
	rm -f "$(DESTDIR)$(PREFIX)/share/doc/stardis-green/README.md"

################################################################################
# Miscellaneous targets
################################################################################
all: build_library build_tests

clean:
	rm -f $(OBJ) $(HDR) .config sgreen doc/sgreen.1

distclean: clean
	rm -f $(DEP)

lint: man
	shellcheck -o all make.sh
	mandoc -Tlint -Wall doc/sgreen.1 || [ $$? -le 1 ]
	mandoc -Tlint -Wall doc/sgreen-input.5 || [ $$? -le 1 ]
	mandoc -Tlint -Wall doc/sgreen-output.5 || [ $$? -le 1 ]
